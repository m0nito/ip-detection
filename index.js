// const mongoose = require('mongoose')
const app = require('express')()

// const { Vps } = require('./db')
// const { port, dbConfig, dbHost } = require('./config')
const { port } = require('./config')

// TODO move to core
/*
app.use(async (req, res, next) => {
    const apiKey = req.headers['x-api-key']
    const vps = await Vps.findOne({ apiKey })
    if (vps) {
        Vps.updateOne({ apiKey }, { calls: (vps.calls += 1) }).exec()
        next()
    } else {
        res.status(403).send({ message: 'Forbidden', code: 403 })
    }
})
*/

app.get('/', (req, res) => {
    const ip = req.headers['x-forwarded-for'] ?
        req.headers['x-forwarded-for'].split(/\s*,\s*/)[0] :
        (req.socket.remoteAddress).replace(/^.*:/, '')

    res.send({ ip })
})

const run = async () => {
    // await mongoose.connect(dbHost, dbConfig)
    app.listen(port, () => {
        console.log(`app started at port ${port}`)
    })
}

run()

