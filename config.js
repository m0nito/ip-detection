module.exports = {
    port: '8080',
    dbHost: 'mongodb://localhost/test',
    dbConfig: { useNewUrlParser: true, useUnifiedTopology: true }
}