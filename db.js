const mongoose = require('mongoose')


exports.Vps = mongoose.model('Vps', {
    id: { type: String, required: true },
    apiKey: { type: String, default: () => uuid.v4() },
    created: { type: Date, default: Date.now },
    modified: { type: Date, default: Date.now },
    calls: { type: Number, default: 0 },
})